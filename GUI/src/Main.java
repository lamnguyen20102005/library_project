
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;

public abstract class Main implements ActionListener {

    private JFrame mainFrame;
    private JButton teacherButton;
    private JButton studentButton;

    public Main() {
        initComponents();
    }

    private void initComponents() {
      
        mainFrame = new JFrame();
        mainFrame.setTitle("EHS Library");
        teacherButton = new JButton("Teacher");
        studentButton = new JButton("Student");

        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(300, 200);
        mainFrame.setLocationRelativeTo(null); // center the frame on the screen

        JLabel welcomeLabel = new JLabel("Welcome to EHS Library!");
        JPanel panel = new JPanel();
        panel.setLayout(null);
        
        welcomeLabel.setBounds(65, 20, 200, 25);
        panel.add(welcomeLabel);

        teacherButton.setBounds(30, 50,  100, 40);
        panel.add(teacherButton);
        studentButton.setBounds(170, 50, 100, 40);
        panel.add(studentButton);
        

        mainFrame.add(panel);
        mainFrame.setVisible(true);

        studentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showLoginForm("Student Login");
            }
        });

        teacherButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showLoginForm("Teacher Login");
            }
        });
    }

    private void showLoginForm(String title) {
        mainFrame.dispose();
        JFrame loginFrame = new JFrame(title);
        JLabel userLabel = new JLabel("User:");
        JTextField userText = new JTextField(20);
        JLabel passwordLabel = new JLabel("Password:");
        JPasswordField passwordText = new JPasswordField();
        JButton button = new JButton("Login");
        JLabel success = new JLabel("");

        loginFrame.setSize(350, 200);
        loginFrame.setLocationRelativeTo(null); // center the frame on the screen

        JPanel panel = new JPanel();
        panel.setLayout(null);

        userLabel.setBounds(10, 20, 80, 25);
        panel.add(userLabel);

        userText.setBounds(100, 20, 165, 25);
        panel.add(userText);

        passwordLabel.setBounds(10, 60, 80, 25);
        panel.add(passwordLabel);

        passwordText.setBounds(100, 60, 165, 25);
        panel.add(passwordText);

        button.setBounds(10, 110, 80, 25);
        panel.add(button);

        success.setBounds(100, 110, 300, 25);
        panel.add(success);

        loginFrame.add(panel);
        loginFrame.setVisible(true);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String user = userText.getText();
                String password = new String(passwordText.getPassword());

                if (user.equals("lam") && password.equals("ctlt20102005")) {
                    success.setText("Login successful!");
                } else {
                    success.setText("Incorrect username or password.");
                }
            }
        });
    }

    public static void main(String[] args)  {
        new Main() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            }
        };
    }

    }   