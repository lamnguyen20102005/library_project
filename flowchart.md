```mermaid
flowchart TD
    A[Start]
    B{Student}
    C[login School ID]
    D[Open interface]
    E[Borrowing]
    F[login fcps email] 
    A --> B
    B --> C 
    B --> F 
    C --> D 
    F --> D 
    D --> E  
    
